package lkti.wse.elektro.um.mobileapplkti2k17;

import android.content.Intent;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    static final int RC_CONFIG_CLIENT = 111;
    static final int MODE_START_TIMER = 112;
    static final int MODE_SET_STATUS = 113;

    // socket variable
    Socket client;
    PrintWriter writer;
    BufferedReader reader;

    // view variable
    Button next, prev, space;
    TextView status, timer, jenisTimer;

    // thread variable
    Handler mHandler;
    MyTimer countDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        status = (TextView) findViewById(R.id.status);
        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == 1) {
                    setTimer(msg.arg1, msg.obj.toString());
                } else if (msg.what == 2) {
                    startTimer(msg.arg1);
                } else if (msg.what == 3) {
                    pauseTimer();
                } else if (msg.what == 4) {
                    stopTimer();
                } else if (msg.what == 5) {
                    status.setText(msg.obj.toString());
                }
            }
        };

        if (client == null) {
            Intent intent = new Intent(getApplicationContext(), ConfigActivity.class);
            startActivityForResult(intent, RC_CONFIG_CLIENT);
        }

        timer = (TextView) findViewById(R.id.timer);
        jenisTimer = (TextView) findViewById(R.id.jenis_timer);
        next = (Button) findViewById(R.id.next_btn);
        prev = (Button) findViewById(R.id.previous_btn);
        space = (Button) findViewById(R.id.space_btn);
        next.setOnClickListener(this);
        prev.setOnClickListener(this);
        space.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_CONFIG_CLIENT) {
            if (resultCode == RESULT_OK) {
                String ip = data.getStringExtra("ip");
                int port = data.getIntExtra("port", -1);
                new Thread(new ClientThread(ip, port, mHandler)).start();
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(getApplicationContext(), "Good Bye !", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        String msg = "";
        if (v == next) {
            msg = "next";
        } else if (v == prev) {
            msg = "prev";
        } else if (v == space) {
            msg = "space";
        } else return;
        if (writer != null) writer.println(msg);
    }

    class ClientThread implements Runnable {
        private String ip;
        private int port;
        private Handler handler;

        public ClientThread(String ip, int port, Handler handler) {
            this.ip = ip; this.port = port; this.handler = handler;
        }

        @Override
        public void run() {
            try {
                client = new Socket(InetAddress.getByName(ip), port);
                reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
                writer = new PrintWriter(client.getOutputStream(), true);
                writer.println("mobile"); writer.flush();
                handler.obtainMessage(5, -1, -1, "TERHUBUNG").sendToTarget();
                Log.d("ClientThread", "ClientThread must be finished");

                while (true) {
                    String msg = reader.readLine();
                    Log.d("ClientThread", "Message from server: "+msg);
                    if(msg.startsWith("set")) {
                        String[] data = msg.split("_");
                        handler.obtainMessage(1, Integer.parseInt(data[2]), -1, data[1])
                                .sendToTarget();
                    } else if(msg.startsWith("start")) {
                        String[] data = msg.split("_");
                        handler.obtainMessage(2, Integer.parseInt(data[1]), -1, "")
                                .sendToTarget();
                    } else if(msg.equals("pause")) {
                        handler.obtainMessage(3).sendToTarget();
                    } else if(msg.equals("stop")) {
                        handler.obtainMessage(4).sendToTarget();
                    }
                }
            } catch (IOException ie) {
                Log.e("ClientThread", "an error occured: "+ie.toString());
                try {
                    reader.close(); writer.close();
                    client.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    reader = null; writer = null; client = null;
                }
                handler.obtainMessage(5, -1, -1, "OFFLINE").sendToTarget();
            }
        }
    }

    private void setTimer(long limit, String jenis) {
        if (limit <= 0) return;
        String startTimer = (limit/60 < 10? "0"+limit/60 : ""+limit/60)
                +" : "+
                (limit%60 < 10? "0"+limit%60 : ""+limit%60);
        timer.setText(startTimer);
        jenisTimer.setText(jenis);
        countDownTimer = new MyTimer(limit*1000, 1000);
    }

    private void startTimer(long limit) {
        if (countDownTimer != null) {
            countDownTimer = new MyTimer(limit*1000, 1000);
            countDownTimer.start();
            Log.d("MainActivity", "countDownTimer started !");
        }
    }

    private void pauseTimer() {
        if (countDownTimer != null) countDownTimer.cancel();
    }

    private void stopTimer() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
            Log.d("MainActivity", "countDownTimer stoped !");
        }
    }

    class MyTimer extends CountDownTimer {
        public MyTimer(long start, long interval) {
            super(start,interval);
        }
        @Override
        public void onFinish() {
            timer.setText("00 : 00");
        }
        @Override
        public void onTick(long milis) {
            long second = milis/1000;
            String time = (second/60 < 10? "0"+second/60 : second/60)+" : ";
            time+= (second%60 < 10? "0"+second%60 : second%60);
            timer.setText(time);
        }
    }

}
