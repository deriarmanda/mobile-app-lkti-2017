package lkti.wse.elektro.um.mobileapplkti2k17;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ConfigActivity extends AppCompatActivity {

    TextInputEditText ipServer, portServer;
    Button connect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        ipServer = (TextInputEditText) findViewById(R.id.ip_field);
        portServer = (TextInputEditText) findViewById(R.id.port_field);
        connect = (Button) findViewById(R.id.connect_btn);

        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ip; int port;
                ip = ipServer.getText().toString();
                if(ip.trim().isEmpty() || portServer.getText().toString().trim().isEmpty()) {
                    AlertDialog alertDialog = new AlertDialog.Builder(ConfigActivity.this).create();
                    alertDialog.setTitle("Alert");
                    alertDialog.setMessage("Alamat IP atau Port tidak boleh kosong !");
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OKE",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                } else {
                    port = Integer.parseInt(portServer.getText().toString());
                    Intent data = new Intent();
                    data.putExtra("ip", ip);
                    data.putExtra("port", port);
                    setResult(RESULT_OK, data);
                    finish();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED, null);
        finish();
    }
}
